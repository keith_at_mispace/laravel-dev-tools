#!/usr/bin/env bash

if [[ -d ./vendor ]]; then

    if [[ -d ./.laravel-dev-tools ]]; then
        (cd ./laravel-dev-tools && git pull)
    else
        git clone ./../laravel-dev-tools .laravel-dev-tools;
    fi

    bash ./.laravel-dev-tools/init-php.sh
fi
